#include <new>
#include <iostream>
#include <ctime>
#include "Engine.h"

int main(int argc, char* args[])
{
	srand(std::time(0));
	
	Engine* engine = nullptr;
	engine = new Engine();
	engine->Start();

	if (engine != nullptr)
	{
		delete engine;
	}

	return 0;
}