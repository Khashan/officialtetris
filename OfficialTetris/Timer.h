#pragma once
class Timer
{
public:
	Timer();
	~Timer();

	///<summary>Start Timer</summary>
	void Start();
	///<summary>Stop Timer</summary>
	void Stop();
	///<summary>Pause Timer</summary>
	void Pause();
	///<summary>Unpause Timer</summary>
	void Unpause();

	///<summary>Get Timer's Ticks</summary>
	int GetTicks();

	///<summary>Is the timer started</summary>
	inline bool IsStarted() const & { return m_IsStarted; }
	///<summary>Is the timer paused</summary>
	inline bool IsPaused() const & { return m_IsPaused; }


private:
	bool m_IsStarted = false;
	bool m_IsPaused = false;

	int m_StartTicks = 0;
	int m_PausedTicks = 0;

};

