#include "Playfield.h"
#include "TetrominoManager.h"
#include "Tetromino.h"
#include "EngineTexture.h"
#include "ParticleManager.h"

#define PARTICLE_ANIM_PATH "animations/TetrisHardDropResized.png"


int Playfield::m_PositionX = 0;
int Playfield::m_PlayfieldWidth = 0;
vector<vector<Playfield::BlockData>> Playfield::m_GameGrid;
vector<Engine::CallbackStruct<void(int&)>> Playfield::m_OnLineCleared;

Playfield::Playfield()
{
	Initialize();
}

Playfield::~Playfield()
{
	Playfield::m_OnLineCleared.clear();
}

void Playfield::Draw(SDL_Renderer* a_Renderer)
{
	//Draw all the BlockData inside the GameGrid
	for (int i = 0; i < m_GameGrid.size(); i++)
	{
		for (int j = 0; j < m_GameGrid[0].size(); j++)
		{
			BlockData& data = m_GameGrid[i][j];
			SDL_Rect& rect = data.a_Rect;

			if (rect.w == 0)
			{
				rect.w = TetrominoManager::GetBlockSize() - 2;
				rect.h = TetrominoManager::GetBlockSize() - 2;
				
				rect.x = m_PositionX + j * TetrominoManager::GetBlockSize() + 1;
				rect.y = i * TetrominoManager::GetBlockSize() + 1;
				data.a_Rect = rect;
			}

			SDL_SetRenderDrawColor(a_Renderer, data.a_Color.r, data.a_Color.g, data.a_Color.b, 0xFF);
			SDL_RenderFillRect(a_Renderer, &rect);
		}
	}
}


void Playfield::Initialize()
{
	//Border
	BlockData b{ false, m_BorderColor, SDL_Rect{0,0,0,0} };
	
	//Empty
	BlockData e{ true, SDL_Color{0,0,0}, SDL_Rect{0,0,0,0} };

	Playfield::m_GameGrid =
	{
		{b, e, e, e, e, e, e, e, e, b},
		{b, e, e, e, e, e, e, e, e, b},
		{b, e, e, e, e, e, e, e, e, b},
		{b, e, e, e, e, e, e, e, e, b},
		{b, e, e, e, e, e, e, e, e, b},
		{b, e, e, e, e, e, e, e, e, b},
		{b, e, e, e, e, e, e, e, e, b},
		{b, e, e, e, e, e, e, e, e, b},
		{b, e, e, e, e, e, e, e, e, b},
		{b, e, e, e, e, e, e, e, e, b},
		{b, e, e, e, e, e, e, e, e, b},
		{b, e, e, e, e, e, e, e, e, b},
		{b, e, e, e, e, e, e, e, e, b},
		{b, e, e, e, e, e, e, e, e, b},
		{b, e, e, e, e, e, e, e, e, b},
		{b, e, e, e, e, e, e, e, e, b},
		{b, e, e, e, e, e, e, e, e, b},
		{b, e, e, e, e, e, e, e, e, b},
		{b, e, e, e, e, e, e, e, e, b},
		{b, b, b, b, b, b, b, b, b, b},
	};

	//Init Static informations
	Playfield::m_PlayfieldWidth = m_GameGrid[0].size() * TetrominoManager::GetBlockSize() - 2;
	Playfield::m_PositionX = (Engine::GetInstance()->GetScreenWidth() * 0.5) - (m_PlayfieldWidth * 0.5);

}

void Playfield::AddTetro(Tetromino* a_NewTetro)
{
	vector<SDL_Rect> blocks = a_NewTetro->GetBlocks();
	for (int i = 0; i < blocks.size(); i++)
	{
		SDL_Rect& block = blocks[i];
		Playfield::m_GameGrid[a_NewTetro->GetYToGame(block)][a_NewTetro->GetXToGame(block)] = BlockData{ false, a_NewTetro->GetColor(), SDL_Rect{0,0,0,0} };
	}

	Playfield::LineCheck();
}

bool Playfield::CollidedWith(Tetromino* a_Tetro, vector<SDL_Rect>& m_Blocks , const int& a_MoveX, const int& a_MoveY)
{
	bool collided = false;

	//Verify if any block of the tetro hits with another block in the gamegrid
	for (int i = 0; i < m_Blocks.size(); i++)
	{
		SDL_Rect& block = m_Blocks[i];

		if (!Playfield::GetPlayGrid()[a_Tetro->GetYToGame(block) + a_MoveY][a_Tetro->GetXToGame(block) + a_MoveX].a_IsEmpty)
		{
			collided = true;
			break;
		}
	}

	return collided;
}

//Ghislain Th�riault's psudo code -> Pretty much what I did but cleaner.
void Playfield::LineCheck()
{
	UpdateGameGridRects();
	BlockData e{ true, SDL_Color{0,0,0}, SDL_Rect{0,0,0,0} };
	int destroyedRows = 0;

	EngineTexture::EngineSpriteStruct sprite = { 6, 6, 0.01f };


	for (int i = m_GameGrid.size() - 2; i >= 0; i--)
	{
		bool IsLineFull = true;

		for (int j = 0; j < m_GameGrid[0].size(); j++)
		{
			if (m_GameGrid[i][j].a_IsEmpty)
			{
				IsLineFull = false;
				break;
			}
		}

		if (IsLineFull)
		{
			for (int x = 1; x < m_GameGrid[0].size() - 1; x++)
			{
				ParticleManager::GetInstance()->CreateParticle(PARTICLE_ANIM_PATH, sprite, m_GameGrid[i][x].a_Rect.x, m_GameGrid[i][x].a_Rect.y);
			}
		}
	}


	//Check if a line is full
	for (int i = m_GameGrid.size() - 2; i >= 0; i--)
	{
		bool IsLineFull = true;

		for (int j = 0; j < m_GameGrid[0].size(); j++)
		{
			if (m_GameGrid[i][j].a_IsEmpty)
			{
				IsLineFull = false;
				break;
			}
		}

		//Clear the Line
		if (IsLineFull)
		{
			for (int y = i; y >= 1; y--)
			{
				for (int x = 1; x < m_GameGrid[0].size() - 1; x++)
				{
					m_GameGrid[y][x] = m_GameGrid[y - 1][x];
					m_GameGrid[y][x].a_Rect = {};
				}
			}
			
			for (int x = 1; x < m_GameGrid[0].size() - 1; x++)
			{
				m_GameGrid[0][x] = e;
			}

			i++;
			destroyedRows++;
		}
	}

	//Call functions in the delegate to announce it
	if (destroyedRows > 0)
	{
		for (int i = 0; i < m_OnLineCleared.size(); i++)
		{
			m_OnLineCleared[i].m_CallBack(destroyedRows);
		}
	}

}

void Playfield::SubscribeOnLineCleared(Engine::CallbackStruct<void(int&)> a_Method)
{
	bool canAdd = true;

	for (int i = 0; i < m_OnLineCleared.size(); i++)
	{
		if (m_OnLineCleared[i].m_ID == a_Method.m_ID)
		{
			canAdd = false;
			break;
		}
	}

	if (canAdd)
	{
		m_OnLineCleared.push_back(a_Method);
	}
}

void Playfield::UnsubscribeOnLineCleared(void* a_Class)
{
	for (int i = 0; i < m_OnLineCleared.size(); i++)
	{
		if (m_OnLineCleared[i].m_ID == a_Class)
		{
			m_OnLineCleared.erase(m_OnLineCleared.begin() + i);
		}
	}
}

void Playfield::UpdateGameGridRects()
{
	//Draw all the BlockData inside the GameGrid
	for (int i = 0; i < m_GameGrid.size(); i++)
	{
		for (int j = 0; j < m_GameGrid[0].size(); j++)
		{
			BlockData& data = m_GameGrid[i][j];
			SDL_Rect& rect = data.a_Rect;

			if (rect.w == 0)
			{
				rect.w = TetrominoManager::GetBlockSize() - 2;
				rect.h = TetrominoManager::GetBlockSize() - 2;

				rect.x = m_PositionX + j * TetrominoManager::GetBlockSize() + 1;
				rect.y = i * TetrominoManager::GetBlockSize() + 1;
				data.a_Rect = rect;
			}
		}
	}
}