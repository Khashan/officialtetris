#include "EngineBehaviour.h"
#include "Engine.h"

#include <iostream>

EngineBehaviour::EngineBehaviour()
{
	Engine::CallbackStruct<void()> upStruct = { std::bind(&EngineBehaviour::Update, this), this, this };
	Engine::CallbackStruct<void(SDL_Renderer*)> drawStruct = { std::bind(&EngineBehaviour::Draw, this, std::placeholders::_1), this, this };
	Engine::CallbackStruct<void(SDL_Event&)> evStruct = { std::bind(&EngineBehaviour::EventsHandler, this, std::placeholders::_1), this, this };

	Engine::GetInstance()->SubscribeCallbacks(upStruct, evStruct, drawStruct);
}

EngineBehaviour::~EngineBehaviour()
{
	Engine::GetInstance()->UnsubscribeCallbacks(this);
}

void EngineBehaviour::Draw(SDL_Renderer* a_Renderer) 
{
	//Default function called -> Remove it from the callback
	Engine::GetInstance()->UnsubscribeDrawCallbacks(this); 
}

void EngineBehaviour::Update() 
{
	//Default function called -> Remove it from the callback
	Engine::GetInstance()->UnsubscribeUpdateCallbacks(this);
}

void EngineBehaviour::EventsHandler(SDL_Event& a_Event)
{
	//Default function called -> Remove it from the callback
	Engine::GetInstance()->UnsubscribeEventHandlerCallbacks(this);
}