#include "EngineTexture.h"
#include "Engine.h"
#include <SDL_image.h>

EngineTexture::EngineTexture()
{
	Free();
}

EngineTexture::~EngineTexture()
{
	Free();
}

bool EngineTexture::LoadFromFile(std::string a_Path)
{
	//Get rid of preexisting texture
	Free();

	//The final texture
	SDL_Texture* newTexture = nullptr;

	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load(a_Path.c_str());
	if (loadedSurface == nullptr)
	{
		printf("Unable to load image %s! SDL_image Error: %s\n", a_Path.c_str(), IMG_GetError());
	}
	else
	{
		//Color key image
		SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0xFF));

		//Create texture from surface pixels
		newTexture = SDL_CreateTextureFromSurface(Engine::GetInstance()->GetRenderer(), loadedSurface);
		if (newTexture == nullptr)
		{
			printf("Unable to create texture from %s! SDL Error: %s\n", a_Path.c_str(), SDL_GetError());
		}
		else
		{
			//Get image dimensions
			m_Width = loadedSurface->w;
			m_Height = loadedSurface->h;
		}

		//Get rid of old loaded surface
		SDL_FreeSurface(loadedSurface);
	}

	//Return success
	m_Texture = newTexture;
	return m_Texture != nullptr;
}

void EngineTexture::Free()
{
	if (m_Texture != nullptr)
	{
		SDL_DestroyTexture(m_Texture);
		m_Texture = nullptr;
		m_Width = 0;
		m_Height = 0;
	}
}

void EngineTexture::SetColor(SDL_Color& a_Color)
{
	//Modulate texture rgb
	SDL_SetTextureColorMod(m_Texture, a_Color.r, a_Color.g, a_Color.b);
}

void EngineTexture::SetBlendMode(SDL_BlendMode& a_Blending)
{
	//Set blending function
	SDL_SetTextureBlendMode(m_Texture, a_Blending);
}

void EngineTexture::SetAlpha(Uint8 alpha)
{
	//Modulate texture alpha
	SDL_SetTextureAlphaMod(m_Texture, alpha);
}

void EngineTexture::DrawTexture(int a_X, int a_Y, SDL_Rect* a_Clip)
{
	//Set rendering space and render to screen
	SDL_Rect renderQuad = { a_X, a_Y, m_Width, m_Height };

	//Set clip rendering dimensions
	if (a_Clip != nullptr)
	{
		renderQuad.w = a_Clip->w;
		renderQuad.h = a_Clip->h;
	}

	//Render to screen
	SDL_RenderCopy(Engine::GetInstance()->GetRenderer(), m_Texture, a_Clip, &renderQuad);
	
	a_Clip = nullptr;
}
