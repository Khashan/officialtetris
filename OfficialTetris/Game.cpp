#include "Game.h"
#include "TetrominoManager.h"
#include "Tetromino.h"
#include "Engine.h"
#include "Playfield.h"
#include "InGameUI.h"
#include "Timer.h"
#include "ParticleManager.h"

#include <SDL.h>
#include <ctime>
#include <new>

#define BACKGROUND_PATH "images/background.jpg"

bool Game::m_IsGameRunning = false;
Game::Game()
{
	m_BackgroundImg = Engine::GetInstance()->LoadSurface("images/background.jpg");
	m_ScaledBg = { 0, 0, Engine::GetInstance()->GetScreenWidth(), Engine::GetInstance()->GetScreenHeight() };

	//Inits
	m_TetroManager = new TetrominoManager(this);
	m_Playfield = new Playfield();
	m_Playfield->SubscribeOnLineCleared(Engine::CallbackStruct<void(int&)> {std::bind(&Game::CompletedRows, this, std::placeholders::_1), this});
	m_InGameUI = new InGameUI();
	m_GameTime = new Timer();
}


Game::~Game()
{
	m_Playfield->UnsubscribeOnLineCleared(this);

	SDL_DestroyTexture(m_BackgroundImg);

	if (m_TetroManager != nullptr)
	{
		delete m_TetroManager;
		m_TetroManager = nullptr;
	}

	if (m_CurrentTetro != nullptr)
	{
		delete m_CurrentTetro;
		m_CurrentTetro = nullptr;
	}

	if (m_NextTetro != nullptr)
	{
		delete m_NextTetro;
		m_NextTetro = nullptr;
	}

	if (m_GameTime != nullptr)
	{
		delete m_GameTime;
		m_GameTime = nullptr;
	}
}

void Game::Draw(SDL_Renderer* a_Renderer)
{
	//Draw order
	if (m_Playfield != nullptr)
		m_Playfield->Draw(a_Renderer);

	if (m_CurrentTetro != nullptr)
		m_CurrentTetro->DrawTetro(a_Renderer);

	if (m_InGameUI != nullptr)
		m_InGameUI->ShowUI(a_Renderer);


	//Change bg color depended of the level
	if (m_CurrentLevel <= m_MaxLevel * .25)
	{
		SDL_SetRenderDrawColor(a_Renderer, 9, 32, 47, 0xFF);
	}
	if (m_CurrentLevel > m_MaxLevel * .25 && m_CurrentLevel <= m_MaxLevel * .5)
	{
		SDL_SetRenderDrawColor(a_Renderer, 5, 19, 28, 0xFF);
	}
	if (m_CurrentLevel > m_MaxLevel * .5 && m_CurrentLevel <= m_MaxLevel * .9)
	{
		SDL_SetRenderDrawColor(a_Renderer, 3, 13, 19, 0xFF);
	}
	if (m_CurrentLevel > m_MaxLevel * .9)
	{
		SDL_SetRenderDrawColor(a_Renderer, 0, 4, 6, 0xFF);
	}
}

void Game::EventsHandler(SDL_Event& a_Event)
{
	switch (a_Event.key.keysym.sym)
	{
	case SDLK_SPACE:
		if (!m_IsGameRunning)
		{
			StartGame();
		}
		break;

	case SDLK_ESCAPE:
		if (m_IsGameRunning)
		{
			m_IsGamePaused = !m_IsGamePaused;
			m_InGameUI->EnablePause(m_IsGamePaused);

			if (m_CurrentTetro != nullptr)
			{
				m_CurrentTetro->SetEnable(!m_IsGamePaused);
			}
		}
		break;
	case SDLK_p:
		if (m_IsGameRunning)
		{
			m_ScoreMultiplier++;
			std::cout << "Cheat activated -> Multiplier " << m_ScoreMultiplier << "\n";
		}
		else
		{
			std::cout << "You need to start the game first";
		}
		break;
	}
}

void Game::Update()
{
	if (m_IsGamePaused)
		return;

	if (m_IsGameRunning)
	{
		//Generate new tetro if empty
		if (m_NextTetro == nullptr)
		{
			m_NextTetro = m_TetroManager->CreateRandomPiece();
			m_NextTetro->SetEnable(false);
			m_InGameUI->ChangeNextTetro(m_NextTetro->GetTypeId());
		}

		ReleaseNextTetro();

		//In-game time
		if (m_GameTime->GetTicks() > 1000)
		{
			m_GameTime->Start();
			m_InGameTime++;
			m_InGameUI->ChangeTimerText(std::to_string(m_InGameTime));
		}

	}
	else if (!m_InGameUI->IsGameOverActivated() && m_GameTime->IsStarted())
	{
		EndGame();
	}
}

void Game::SetGameRunning(bool a_IsRunning)
{
	m_IsGameRunning = a_IsRunning;
}

void Game::CompletedRows(int a_TotalDestroyed)
{
	//Update Score
	switch (a_TotalDestroyed)
	{
	case 1:
		m_Scores += (40 * (m_CurrentLevel + 1)) * m_ScoreMultiplier;
		break;
	case 2:
		m_Scores += (100 * (m_CurrentLevel + 1)) * m_ScoreMultiplier;
		break;
	case 3:
		m_Scores += (300 * (m_CurrentLevel + 1)) * m_ScoreMultiplier;
		break;
	case 4:
		m_Scores += (1200 * (m_CurrentLevel + 1)) * m_ScoreMultiplier;
		break;
	}

	m_DestroyedLines += a_TotalDestroyed * m_ScoreMultiplier;

	//Update Level if possible.
	if (m_CurrentLevel != m_MaxLevel)
	{
		m_CurrentLevel = ((m_DestroyedLines * 0.1f) + 1 > m_MaxLevel) ? m_MaxLevel : (m_DestroyedLines * 0.1f) + 1;
	}

	//Update UI
	m_InGameUI->ChangeScoreText(std::to_string(m_Scores));
	m_InGameUI->ChangeLevelText(std::to_string(m_CurrentLevel));
	m_InGameUI->ChangeLineClearedText(std::to_string(m_DestroyedLines));
}

void Game::ReleaseNextTetro()
{
	//Verification to delete current tetro
	if (m_CurrentTetro != nullptr && !m_CurrentTetro->IsEnable())
	{
		delete m_CurrentTetro;
		m_CurrentTetro = nullptr;
	}

	//Init currentTetro with the next one
	if (m_CurrentTetro == nullptr)
	{
		m_CurrentTetro = m_NextTetro;
		m_CurrentTetro->SetEnable(true);
		m_NextTetro = nullptr;
	}
}

void Game::EndGame()
{
	//Show end screen
	m_InGameUI->EnableGameOver(true, m_Scores);

	//Clear pointers
	if (m_CurrentTetro != nullptr)
	{
		delete m_CurrentTetro;
		m_CurrentTetro = nullptr;
	}

	if (m_NextTetro != nullptr)
	{
		delete m_NextTetro;
		m_NextTetro = nullptr;
	}
}

void Game::StartGame()
{
	//Init Game Setting
	m_InGameTime = 0;
	m_Scores = 0;
	m_CurrentLevel = 1;
	m_ScoreMultiplier = 1;
	m_DestroyedLines = 0;
	m_InGameUI->EnableGameOver(false, m_Scores);
	m_Playfield->Initialize();
	m_IsGameRunning = true;
	m_GameTime->Start();

	//Update UI
	m_InGameUI->ChangeScoreText(std::to_string(m_Scores));
	m_InGameUI->ChangeLevelText(std::to_string(m_CurrentLevel));
	m_InGameUI->ChangeTimerText(std::to_string(m_InGameTime));
	m_InGameUI->ChangeLineClearedText(std::to_string(m_DestroyedLines));

	ParticleManager::GetInstance()->ClearParticles();
}