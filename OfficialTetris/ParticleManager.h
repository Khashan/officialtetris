#pragma once
#include <vector>
#include <string>
#include "EngineTexture.h"

using std::vector;
using std::string;

class Particle;

class ParticleManager
{
public:
	ParticleManager();
	~ParticleManager();

	///<summary>Get Singleton particule</summary>
	static ParticleManager* GetInstance();

	///<summary>Create a particule at a pos</summary>
	///<param name="ImgPath"> Path to the spritesheet </param>
	///<param name="Settings"> Settings of the EngineSpriteStruct </param>
	///<param name="X">Position axis-X</param>
	///<param name="Y">Position axis-Y</param>
	void CreateParticle(string a_ImgPath, EngineTexture::EngineSpriteStruct& a_Settings, int a_X, int a_Y);

	///<summary>Remove  Dead Particles</summary>
	void RemoveParticle(Particle* a_DeadParticle);

	///<summary>Clear all particules and delete their pointers</summary>
	void ClearParticles();

private:
	static ParticleManager* s_Instance;
	vector<Particle*> m_ActiveParticles;
};

