#pragma once
#include <SDL.h>
#include "EngineBehaviour.h"

class Tetromino;
class TetrominoManager;
class Playfield;
class InGameUI;
class Timer;

class Game : public EngineBehaviour
{
public:
	Game();
	~Game();

	///<summary>Get Tetris' current Level</summary>
	int const inline & GetLevel() { return m_CurrentLevel; }

	///<summary>Change GameState if it's still running</summary>
	/// <param name="a_IsRunning">Used to indicate status.</param>
	static void SetGameRunning(bool a_IsRunning);

private:
	///<summary>Called when a piece is placed in the GameGrid</summary>
	/// <param name="a_TotalDestroyed">How many line as been destroyed.</param>
	void CompletedRows(int a_TotalDestroyed);

	///<summary>Draw the component inside the window</summary>
	/// <param name="a_Renderer">Main renderer system.</param>
	void Draw(SDL_Renderer* a_Renderer) override;
	
	///<summary>Update the component</summary>
	void Update() override;

	///<summary>Receive SDL Events</summary>
	/// <param name="a_Event">The SDL event.</param>
	void EventsHandler(SDL_Event& a_Event) override;

	///<summary>Use the next tetromino as the current one</summary>
	void ReleaseNextTetro();

	///<summary>When the game is over</summary>
	void EndGame();

	///<summary>Start the game</summary>
	void StartGame();

#pragma region Vars
	int m_InGameTime = 0;
	int m_DestroyedLines = 0;
	int m_CurrentLevel = 1;
	const int m_MaxLevel = 19;

	int m_Scores = 0;
	int m_ScoreMultiplier = 1;

	static bool m_IsGameRunning;
	bool m_IsGamePaused = false;

	SDL_Rect m_ScaledBg;
	Timer* m_GameTime = nullptr;

	SDL_Texture* m_BackgroundImg = nullptr;
	
	Playfield* m_Playfield = nullptr;
	
	InGameUI* m_InGameUI = nullptr;

	Tetromino* m_CurrentTetro = nullptr;
	Tetromino* m_NextTetro = nullptr;
	TetrominoManager* m_TetroManager = nullptr;
#pragma endregion

};


