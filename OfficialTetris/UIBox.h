#pragma once
#include <SDL.h>
#include <SDL_ttf.h>
#include <string>
#include "EngineBehaviour.h"

using std::string;


class UIBox : public EngineBehaviour
{
public:
	enum BoxStyle { NO_STYLE, BOX, LINE, DOUBLE_LINE };
	enum BoxContenuPos { TOP_LEFT, TOP_CENTER, TOP_RIGHT, MIDDLE_LEFT, MIDDLE_CENTER, MIDDLE_RIGHT, BOTTOM_LEFT, BOTTOM_CENTER, BOTTOM_RIGHT};
	enum BoxBackground { NO_BACKGROUND, COLOR, IMAGE};
	struct Box
	{
		char* m_Title;
		int m_TitleSize;
		char* m_FontPath;
		SDL_Color m_TitleColor;
		SDL_Rect m_Rect;
		SDL_Color m_BackgroundColor, m_BorderColor;
		BoxBackground m_Background;
		BoxStyle m_Style;
		BoxContenuPos m_ContenuPos;
		string m_ImgPath;
	};
	
	/// <param name="Box"> UIBox::Box, Body's data </param>
	explicit UIBox(Box a_Box);
	~UIBox();

	/// <summary>Draw the UIBox inside the window</summary>
	void virtual DrawBox(SDL_Renderer* a_Renderer);

	inline Box& GetBox() { return m_Box; }
	virtual void SetBox(Box& a_NewBox);

protected:
	Box m_Box;

private:
	SDL_Surface* m_SurfaceMessage = nullptr;
	SDL_Texture* m_MessageSurface = nullptr;
	void DrawBackground(SDL_Renderer* a_Renderer);
	void DrawBorder(SDL_Renderer* a_Renderer);
	void DrawTitle(SDL_Renderer* a_Renderer);

	SDL_Texture* m_BackgroundImg = nullptr;
	TTF_Font* m_TitleFont = nullptr;
	SDL_Rect m_Message_rect = SDL_Rect{0,0,0,0};


};

