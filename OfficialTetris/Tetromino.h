#pragma once
#include "EngineBehaviour.h"

#include <vector>
#include <SDL.h>
#include <string>

using std::vector;
using std::string;

class TetrominoManager;
class Timer;

class Tetromino : public EngineBehaviour
{
public:
	///<param name="X">Postion in the Template (Row)</param>
	///<param name="Y">Position in the Template (Column)</param>
	///<param name="Template">Tetro's matrice</param>
	///<param name="Color">Tetro's Color</param>
	///<param name="Level">Game's Level</param>
	explicit Tetromino(int a_TypeID, int a_X, int a_Y, vector<vector<vector<int>>> a_Template, SDL_Color a_Color, int a_Level);
	~Tetromino();

	///<summary>Move the Tetromino</summary>
	///<param name="X">Move in the X-Axis</param>
	///<param name="Y">Move in the Y-Axis</param>
	void Move(int a_X, int a_Y);

	///<summary>Draw the component inside the window</summary>
	/// <param name="a_Renderer">Main renderer system.</param>
	void DrawTetro(SDL_Renderer* a_Renderer);

	///<summary>Get the tetromino's color</summary>
	inline const SDL_Color GetColor() { return m_ColorTetro; }

	///<summary>Get the tetromino's Matrice</summary>
	inline const vector<vector<vector<int>>> GetTemplates() const & { return m_Template; }
	///<summary>Get the tetromino's current matrice</summary>
	inline const vector<vector<int>> GetCurrentTemplate() const & { return m_Template[m_CurrentRotation]; }

	///<summary>Get Tetro's SDL_Rects</summary>
	vector<SDL_Rect> GetBlocks() { return m_Blocks; }


	///<summary>Get the block's X position</summary>
	///<param name="Rect">The rect/block you want to know the position</param>
	int GetXToGame(SDL_Rect& a_Rect);

	///<summary>Get the block's Y position</summary>
	///<param name="Rect">The rect/block you want to know the position</param>
	int GetYToGame(SDL_Rect& a_Rect);

	///<summary>Disable/Enable EngineBehaviour</summary>
	void SetEnable(bool a_Enable) override;

	int GetTypeId();

private:
	///<summary>Receive SDL Events</summary>
	/// <param name="a_Event">The SDL event.</param>
	void EventsHandler(SDL_Event& a_Event) override;
	///<summary>Update the component</summary>
	void Update() override;
	///<summary>Rotate Tetromino</summary>
	void Rotate(bool a_Clockwise);
	///<summary>Find the bottom of the gamegrid</summary>
	void UpdateGhostPosition();
	///<summary>Do a hard drop</summary>
	void HardDrop();

	int m_ID = -1;
	Timer* m_MoveTimer = nullptr;
	int m_Level = 1;

	int m_DefaultX, m_DefaultY;
	int m_PosX, m_PosY;
	int m_CurrentRotation = 0;

	vector<SDL_Rect> m_Blocks;
	vector<SDL_Rect> m_GhostBlocks;

	SDL_Color m_ColorTetro;
	SDL_Color m_ColorGhostTetro;
	vector<vector<vector<int>>> m_Template;
};
