#include "Timer.h"
#include <SDL.h>


Timer::Timer()
{
}


Timer::~Timer()
{
}


void Timer::Start()
{
	m_IsStarted = true;
	m_IsPaused = false;

	m_StartTicks = SDL_GetTicks();
}

void Timer::Stop()
{
	m_IsStarted = false;
	m_IsPaused = true;
}

void Timer::Pause()
{
	if (m_IsStarted && !m_IsPaused)
	{
		m_IsPaused = false;

		m_PausedTicks = SDL_GetTicks() - m_StartTicks;
	}
}

void Timer::Unpause()
{
	if (m_IsPaused)
	{
		m_IsPaused = false;

		m_StartTicks = SDL_GetTicks() - m_PausedTicks;

		m_PausedTicks = 0;
	}
}

int Timer::GetTicks()
{
	if (m_IsStarted)
	{
		if (m_IsPaused)
		{
			return m_PausedTicks;
		}
		else
		{
			return SDL_GetTicks() - m_StartTicks;
		}
	}

	return 0;
}