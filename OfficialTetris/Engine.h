#pragma once
#include <SDL.h>
#include <SDL_image.h>
#include <string>
#include <iostream>
#include <vector>
#include <functional>
#include <string>

using std::vector;

using std::function;
using std::string;

class Timer;
class Game;
class EngineBehaviour;
class ParticleManager;

class Engine
{
public:
	template <class T>
	struct CallbackStruct {
		//the callback - takes a uint32_t input.
		std::function<T> m_CallBack;
		//value to return with the callback.
		void* m_ID = nullptr;
		EngineBehaviour* m_Behaviour = nullptr;
	};

	Engine();
	~Engine();

	/// <summary>Start the Engine loop</summary>
	void Start();
	
	/// <summary>First call to Draw inside the window</summary>
	void Draw();
	
	/// <summary>Manager of the SDL - Events</summary>
	void EventsHandler();
	
	/// <summary>First call of the Update's GameLoop</summary>
	void Update();
	
	/// <summary>Load image into a surface.</summary>
	SDL_Texture* LoadSurface(std::string path);
	
	/// <summary>Get Engine's Singleton</summary>
	inline static Engine* GetInstance() { return s_Instance; }

	/// <summary>Get Screen Width</summary>
	int inline const GetScreenWidth() { return SCREEN_WIDTH; }
	
	/// <summary>Get Screen Height</summary>
	int inline const GetScreenHeight() { return SCREEN_HEIGHT; }


	/// <summary>Get Engine's renderer</summary>
	inline SDL_Renderer* GetRenderer() { return m_Renderer; }

	/// <summary>Subscribe/Listen to the main Delegates </summary>
	/// <param name="a_Update">CallbackStruct for Update function</param>
	/// <param name="a_EventFunc">CallbackStruct for EventHandler function</param>
	/// <param name="a_DrawFunc">CallbackStruct for Draw function</param>
	void SubscribeCallbacks(CallbackStruct<void()>& a_Update, CallbackStruct<void(SDL_Event&)>& a_EventFunc, CallbackStruct<void(SDL_Renderer*)>& a_DrawFunc);
	
	/// <summary>Unsubscribe from the main delegates</summary>
	/// <param name= "a_Class">Pointer of the class</param>
	void UnsubscribeCallbacks(void* a_Class);

	/// <summary>Unsubscribe from the main delegates</summary>
	/// <param name= "a_Update">Update Callstruct</param>
	void UnsubscribeUpdateCallbacks(void* a_Class);

	/// <summary>Unsubscribe from the main delegates</summary>
	/// <param name= "a_EventFunc">EventHandler Callstruct</param>
	void UnsubscribeEventHandlerCallbacks(void* a_Class);

	/// <summary>Unsubscribe from the main delegates</summary>
	/// <param name= "a_DrawFunc">Draw CallStruct</param>
	void UnsubscribeDrawCallbacks(void* a_Class);

private:
	/// <summary>Initialize all the engine Componenets / SDL libs</summary>
	bool Init();
	static Engine* s_Instance;

	Timer* m_Fps;
	Timer* m_CapTimer;
	int m_Frame = 0;
	
	//Engine Settings
	const int SCREEN_FPS = 60;
	const int SCREEN_TICK_PER_FRAME = 1000 / SCREEN_FPS;
	const int TOTAL_PARTICLES = 40;
	bool m_IsCap = true;

	//Screen dimension constants
	const int SCREEN_WIDTH = 700;
	const int SCREEN_HEIGHT = 800;
	const char* SCREEN_NAME = "Simon Anderson - Tetris V2";
	
	vector<CallbackStruct<void()>> m_UpdateDelegate;
	vector<CallbackStruct<void(SDL_Event&)>> m_EventsDelegate;
	vector<CallbackStruct<void(SDL_Renderer*)>> m_DrawDelegates;

	bool m_IsEngineRunning;

	SDL_Window* m_Window = nullptr;
	SDL_Surface* m_ScreenSurface = nullptr;
	SDL_Renderer* m_Renderer = nullptr;
	ParticleManager* m_ParticleManager = nullptr;

	Game* m_Game = nullptr;
};
