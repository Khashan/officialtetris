#include "ParticleManager.h"
#include "Particle.h"

ParticleManager* ParticleManager::s_Instance;
ParticleManager::ParticleManager()
{
	if (ParticleManager::GetInstance() == nullptr)
	{
		s_Instance = this;
	}
}

ParticleManager::~ParticleManager()
{
	for (int i = 0; i < m_ActiveParticles.size(); i++)
	{
		delete m_ActiveParticles[i];
	}

	m_ActiveParticles.clear();
	s_Instance = nullptr;
}

ParticleManager* ParticleManager::GetInstance()
{
	return s_Instance;
}

void ParticleManager::CreateParticle(string a_ImgPath, EngineTexture::EngineSpriteStruct& a_Settings, int a_X, int a_Y)
{
	m_ActiveParticles.push_back(new Particle(a_ImgPath, a_Settings, a_X, a_Y));
}

void ParticleManager::RemoveParticle(Particle* a_DeadParticle)
{
	for (int i = 0; i < m_ActiveParticles.size(); i++)
	{
		if (m_ActiveParticles[i] == a_DeadParticle)
		{
			delete a_DeadParticle;
			m_ActiveParticles[i] = nullptr;
			m_ActiveParticles.erase(m_ActiveParticles.begin() + i);
			break;
		}
	}
}

void ParticleManager::ClearParticles()
{
	for (int i = 0; i < m_ActiveParticles.size(); i++)
	{
		delete m_ActiveParticles[i];
	}

	m_ActiveParticles.clear();
}