#pragma once
#include <vector>
#include <SDL.h>
#include "Engine.h"

using std::vector;

class Tetromino;
class Engine;

class Playfield
{
public:
	struct BlockData
	{
		bool a_IsEmpty;
		SDL_Color a_Color;
		SDL_Rect a_Rect;
	};

	Playfield();
	~Playfield();

	///<summary>Draw the component inside the window</summary>
	/// <param name="a_Renderer">Main renderer system.</param>
	void Draw(SDL_Renderer* a_Renderer);

	///<summary>Initialize the Playfield's variables </summary>
	void Initialize();

	///<summary>Get the Origin-X position of the GameGrid</summary>
	static int const inline& GetPositionX() { return m_PositionX; }

	///<summary>Get the playfield Width</summary>
	static int const inline& GetPlayfieldWidth() { return m_PlayfieldWidth; }

	///<summary>Get the PlayGrid</summary>
	static vector<vector<BlockData>> const inline& GetPlayGrid() { return m_GameGrid; }
	
	///<summary>Verify if you are colliding with the GameGrid</summary>
	///<param name="Tetro">Current Tetro</param>
	///<param name="Blocks">Tetromino's blocks to verify with</param>
	///<param name="MoveX">How many Move to the X-axis</param>
	///<param name="MoveY">How many Move to the Y-axis</param>
	static bool CollidedWith(Tetromino* a_Tetro,  vector<SDL_Rect>& a_Blocks, const int& a_MoveX, const int& a_MoveY);

	///<summary>Add the current Tetromino inside the GameGrid</summary>
	///<param name="NewTetro">Current Tetromino</param>
	static void AddTetro(Tetromino* a_NewTetro);
	
	///<summary>Subscribe to OnLineCleared's Delegate</summary>
	///<param name="Method">The Engine::CallbackStruct to the function</param>
	static void SubscribeOnLineCleared(Engine::CallbackStruct<void(int&)> a_Method);

	///<summary>Unsubscribe to OnLineCleared's Delegate</summary>
	///<param name="Method">Class Pointer</param>
	static void UnsubscribeOnLineCleared(void* a_Class);

private:
	static void UpdateGameGridRects();

	///<summary>Verify if any lines are completed.</summary>
	static void LineCheck();

#pragma region Vars
	SDL_Color m_BorderColor{ 255,255,255 };

	static int m_PositionX;
	static int m_PlayfieldWidth;

	static vector<vector<BlockData>> m_GameGrid;
	static vector<Engine::CallbackStruct<void(int&)>> m_OnLineCleared;

	int m_LinesToDestroy = 0;
#pragma endregion
};

