#pragma once
#include "UIBox.h"

using std::string;

class MessageBox : public UIBox
{
public:
	///<param name="Box">UIBox::Box , the body's data of the box</param>
	///<param name="TextSize">Font Size</param>
	///<param name="TextColor">Font color (SDL_COLOR)</param>
	explicit MessageBox(Box a_Box, string a_Text, int a_TextSize, SDL_Color a_TextColor);
	~MessageBox();

	///<summary>Draw the component inside the window</summary>
	/// <param name="a_Renderer">Main renderer system.</param>
	void DrawBox(SDL_Renderer* a_Renderer);

	///<summary>Change the Text of the box</summary>
	/// <param name="NewText">The new text for the box.</param>
	void ChangeText(string a_NewText);


	///<summary>Resize Rect size</summary>
	void ResizeRect();

private:
	string m_MessageText;
	TTF_Font* m_MessageFont = nullptr;
	SDL_Texture* m_MessageSurface = nullptr;
	SDL_Surface* m_SurfaceMessage = nullptr;
	SDL_Rect m_Message_rect{};
	SDL_Color m_TextColor{};


};

