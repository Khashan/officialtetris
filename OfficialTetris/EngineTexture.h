#pragma once
#include <SDL.h>
#include <string>
#include <vector>

#include "EngineBehaviour.h"

using std::vector;

class EngineTexture : public EngineBehaviour
{
public:
	struct EngineSpriteStruct
	{
		int m_TotalXSprites;
		int m_TotalYSprites;
		float m_AnimationDelay;
		SDL_Rect m_SpriteSettings;
		bool m_HasLoop = false;
		int m_AnimationLifeTime = -1; // -1 = infinite life time
		vector<SDL_Rect> m_SpriteClips;
		int m_TotalSprites;
		SDL_Rect m_CurrentClip;
	};

	EngineTexture();
	~EngineTexture();

	///<summary>Load Image form File</summary>
	bool LoadFromFile(std::string a_Path);

	///<summary>Deallocates Texture</summary>
	void Free();
	
	///<summary>Set Texture Color</summary>
	void SetColor(SDL_Color& a_Color);

	///<summary>Set Texture BlendMode</summary>
	void SetBlendMode(SDL_BlendMode& a_Blending);

	///<summary>Set Texture Alpha</summary>
	void SetAlpha(Uint8 a_Alpha);

	///<summary>Render Texutre</summary>
	void DrawTexture(int x, int y, SDL_Rect* clip = nullptr);

	///<summary>Get Width of the texture</summary>
	inline int& GetWidth() { return m_Width; }
	///<summary>Get Height of the texture</summary>
	inline int& GetHeight() { return m_Height; }


private:
	int m_Width = 0;
	int m_Height = 0;
	SDL_Texture* m_Texture = nullptr;

};

