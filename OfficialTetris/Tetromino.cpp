#include "Tetromino.h"
#include "Engine.h"
#include "Timer.h"
#include "TetrominoManager.h"
#include "Playfield.h"
#include "Game.h"
#include "Particle.h"
#include "ParticleManager.h"
#include "EngineTexture.h"

#include <new>

#define PARTICLE_ANIM_PATH "animations/TetrisHardDropResized.png"

using namespace std::placeholders;

Tetromino::Tetromino(int a_TypeID, int a_X, int a_Y, vector<vector<vector<int>>> a_Template, SDL_Color a_Color, int a_Level)
	:m_ID(a_TypeID), m_DefaultX(a_X), m_DefaultY(a_Y), m_Template(a_Template), m_ColorTetro(a_Color), m_Level(a_Level)
{
	//Init alpha for ghost block
	m_ColorGhostTetro = a_Color;
	m_ColorGhostTetro.a = 125;

	//Translate matrice to SDL_Rects
	for (int i = 0; i < m_Template[m_CurrentRotation].size(); i++)
	{
		for (int j = 0; j < m_Template[m_CurrentRotation][0].size(); j++)
		{
			if (m_Template[m_CurrentRotation][i][j] != 0)
			{
				int playPosX = (Playfield::GetPlayfieldWidth() * 0.5) + Playfield::GetPositionX();
				m_Blocks.push_back(SDL_Rect{ playPosX + ((i - m_DefaultX) * TetrominoManager::GetBlockSize()) + 2 , (j + m_DefaultY) * TetrominoManager::GetBlockSize() + 1,  TetrominoManager::GetBlockSize() - 2, TetrominoManager::GetBlockSize() - 2 });
			}
		}
	}

	//Clones Blocks
	m_GhostBlocks = m_Blocks;
	
	//Verify if it doesn't collide at the spawn point (EndGame)
	if (Playfield::CollidedWith(this, m_Blocks, 0, 0))
	{
		Game::SetGameRunning(false);
	}

	m_MoveTimer = new Timer();
}


Tetromino::~Tetromino()
{
	if (m_MoveTimer != nullptr)
	{
		delete m_MoveTimer;
	}
}

void Tetromino::DrawTetro(SDL_Renderer* a_Renderer)
{
	//Draw Blocks
	for (SDL_Rect& block : m_Blocks)
	{
		SDL_SetRenderDrawColor(a_Renderer, m_ColorTetro.r, m_ColorTetro.g, m_ColorTetro.b, 0xFF);
		SDL_RenderFillRect(a_Renderer, &block);
	}
	
	for (SDL_Rect& gBlock : m_GhostBlocks)
	{
		SDL_SetRenderDrawColor(a_Renderer, m_ColorTetro.r, m_ColorTetro.g, m_ColorTetro.b, m_ColorGhostTetro.a);
		SDL_RenderFillRect(a_Renderer, &gBlock);
	}
}


void Tetromino::Update()
{
	//Timer to move block with a random formula that doesn't mean anything that I made...
	if (m_MoveTimer->GetTicks() > 1000 - (m_Level * ((m_Level + 2) * 2)))
	{
		//Reset Timer
		m_MoveTimer->Start();

		//Verification for collision
		if (!Playfield::CollidedWith(this, m_Blocks, 0, 1))
		{
			Move(0, 1);
		}
		else
		{
			Playfield::AddTetro(this);
			SetEnable(false);
		}

		//Update Ghost
		UpdateGhostPosition();
	}
}

void Tetromino::EventsHandler(SDL_Event& a_Event)
{
	//Inputs
	switch (a_Event.key.keysym.sym)
	{
	case SDLK_w:
	case SDLK_UP:
		HardDrop();
		break;

	case SDLK_s:
	case SDLK_DOWN:
		Move(0, 1);
		break;
	case SDLK_a:
	case SDLK_LEFT:
		Move(-1, 0);
		break;
	case SDLK_d:
	case SDLK_RIGHT:
		Move(1, 0);
		break;

	case SDLK_q:
		Rotate(true);
		break;

	case SDLK_e:
		Rotate(false);
		break;

	default:
		break;
	}

	UpdateGhostPosition();
}

void Tetromino::Move(int a_X, int a_Y)
{
	if (!Playfield::CollidedWith(this, m_Blocks, a_X, a_Y))
	{
		m_PosX += a_X;
		m_PosY += a_Y;
		for (SDL_Rect& block : m_Blocks)
		{
			block.x += a_X * TetrominoManager::GetBlockSize();
			block.y += a_Y * TetrominoManager::GetBlockSize();
		}
	}
}

void Tetromino::Rotate(bool a_Clockwise)
{
	vector<SDL_Rect> copyBlocks = m_Blocks;

	int currIndex = 0;
	int lastRotation = m_CurrentRotation;

	//Get index of the rotation
	if (a_Clockwise)
		m_CurrentRotation = m_CurrentRotation + 1 == 4 ? 0 : m_CurrentRotation + 1;
	else
		m_CurrentRotation = m_CurrentRotation - 1 < 0 ? 3 : m_CurrentRotation - 1;

	//Update blocks for the new rotation
	for (int i = 0; i < m_Template[m_CurrentRotation].size(); i++)
	{
		for (int j = 0; j < m_Template[m_CurrentRotation][0].size(); j++)
		{
			if (m_Template[m_CurrentRotation][i][j] != 0)
			{
				SDL_Rect& currRect = m_Blocks[currIndex];
				int playPosX = (Playfield::GetPlayfieldWidth() * 0.5) + Playfield::GetPositionX();
				SDL_Rect rotRect = { playPosX + ((i - m_DefaultX) * TetrominoManager::GetBlockSize()) + 2 , (j + m_DefaultY) * TetrominoManager::GetBlockSize() + 1,  TetrominoManager::GetBlockSize() - 2, TetrominoManager::GetBlockSize() - 2 };
				rotRect.x += m_PosX * TetrominoManager::GetBlockSize();
				rotRect.y += m_PosY * TetrominoManager::GetBlockSize();

				m_Blocks[currIndex] = rotRect;
				currIndex++;
			}
		}
	}

	//If new rotation collide -> Return to last rotation index and blocks settings
	if (Playfield::CollidedWith(this, m_Blocks, 0, 0))
	{
		m_Blocks = copyBlocks;
		m_CurrentRotation = lastRotation;
	}

}

int Tetromino::GetXToGame(SDL_Rect& a_Rect)
{
	//Converting Vector to Axis Y
	return (((a_Rect.x + m_DefaultX) - Playfield::GetPositionX()) / (a_Rect.w));
}

int Tetromino::GetYToGame(SDL_Rect& a_Rect)
{
	//Converting Vector to Axis X
	return ((a_Rect.y - m_DefaultY) / (a_Rect.h + 1));
}

void Tetromino::SetEnable(bool a_Enable)
{
	m_IsEnable = a_Enable;

	//Control Timer State
	if (m_IsEnable)
	{
		m_MoveTimer->Start();
	}
	else
	{
		m_MoveTimer->Stop();
	}
}

int Tetromino::GetTypeId()
{
	return m_ID;
}

void Tetromino::UpdateGhostPosition()
{
	//Update Ghost Position
	for (int i = 0; i < m_Blocks.size(); i++)
	{
		m_GhostBlocks[i] = m_Blocks[i];
	}

	//Find valid bottom
	while(!Playfield::CollidedWith(this, m_GhostBlocks, 0, 1))
	{
		for (SDL_Rect& block : m_GhostBlocks)
		{
			block.y += TetrominoManager::GetBlockSize();
		}
	}
}

void Tetromino::HardDrop()
{
	//Security -> Be sure the ghost aren't the same as the real blocks otherwise we can have floating blocks in the GameGrid;
	bool notTheSame = false;

	for (int i = 0; i < m_Blocks.size(); i++)
	{
		if (m_Blocks[i].y != m_GhostBlocks[i].y)
		{
			notTheSame = true;
		}

		m_Blocks[i] = m_GhostBlocks[i];
	}

	if (notTheSame)
	{
		Playfield::AddTetro(this);
		SetEnable(false);
	}
}