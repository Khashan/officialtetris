#include "MessageBox.h"
#include <iostream>


MessageBox::MessageBox(Box a_Box, string a_Text, int a_TextSize, SDL_Color a_TextColor)
	: UIBox(a_Box), m_MessageText(a_Text), m_TextColor(a_TextColor)
{
	m_MessageFont = TTF_OpenFont(m_Box.m_FontPath, a_TextSize);
	m_SurfaceMessage = TTF_RenderText_Solid(m_MessageFont, m_MessageText.c_str(), m_TextColor);
	ResizeRect();
}

MessageBox::~MessageBox()
{
	TTF_CloseFont(m_MessageFont);
	SDL_FreeSurface(m_SurfaceMessage);
	SDL_DestroyTexture(m_MessageSurface);
}

void MessageBox::DrawBox(SDL_Renderer* a_Renderer)
{
	UIBox::DrawBox(a_Renderer);

	if (m_MessageSurface == nullptr)
	{
		m_MessageSurface = SDL_CreateTextureFromSurface(a_Renderer, m_SurfaceMessage);
	}

	SDL_RenderCopy(a_Renderer, m_MessageSurface, nullptr, &m_Message_rect);
}

void MessageBox::ChangeText(string a_NewText)
{
	SDL_FreeSurface(m_SurfaceMessage);
	m_MessageText = a_NewText;
	m_SurfaceMessage = TTF_RenderText_Solid(m_MessageFont, m_MessageText.c_str(), m_TextColor);

	ResizeRect();

	SDL_DestroyTexture(m_MessageSurface);
	m_MessageSurface = nullptr;

}

void MessageBox::ResizeRect()
{
	int tw, th;
	if (!TTF_SizeText(m_MessageFont, m_MessageText.c_str(), &tw, &th))
	{
		m_Message_rect = SDL_Rect{ m_Box.m_Rect.x + (m_Box.m_Rect.w / 2) - (tw / 2), m_Box.m_Rect.y + (m_Box.m_Rect.h / 2) - (th / 2) ,tw, th };
	}
}