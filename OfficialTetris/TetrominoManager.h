#pragma once
#include <map>
#include <vector>

class Tetromino;
class Game;

class TetrominoManager
{
public:
	enum TetrominoType { I, J, L, O, S, T, Z };
	TetrominoManager(Game* a_Game);
	~TetrominoManager();

	///<summary>Craete Random Tetromino</summary>
	Tetromino* CreateRandomPiece();

	///<summary>Get Block size</summary>
	static const int& GetBlockSize() { return m_TetroSize; }

private:
	///<summary>Create Specific Tetromino</summary>
	Tetromino* CreatePiece(TetrominoType aType);
	///<summary>Create Tetrominos Database</summary>
	void CreateDatabase();

	Game* m_Game = nullptr;
	
	int m_TypesCount = 7;
	static int m_TetroSize;
	
	std::map<TetrominoType, std::vector<std::vector<std::vector<int>>>> m_Pieces;
	std::map<TetrominoType, std::vector<int>> m_DefaultPosition;
};
