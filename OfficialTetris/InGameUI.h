#pragma once
#include "EngineBehaviour.h"
#include <string>

using std::string;

class MessageBox;
class UIBox;

class InGameUI : public EngineBehaviour
{
public:
	InGameUI();
	~InGameUI();

	/// <summary>Initializing all In-Game UI components</summary>
	void Init();

	/// <summary>Draw the UI</summary>
	void ShowUI(SDL_Renderer* a_Renderer);

	/// <summary>Change Text of score</summary>
	void ChangeScoreText(string a_Text);
	/// <summary>Change Text of Level</summary>
	void ChangeLevelText(string a_Text);
	/// <summary>Change Text of Timer</summary>
	void ChangeTimerText(string a_Text);	
	/// <summary>Change Text of Timer</summary>
	void ChangeLineClearedText(string a_Text);
	
	/// <summary>Show Pause Screen</summary>
	void EnablePause(bool a_Enable);
	/// <summary>Show GameOver Screen</summary>
	void EnableGameOver(bool a_Enable, int& a_Score);

	/// <summary>Is the GameOver Screen active</summary>
	bool IsGameOverActivated();

	/// <summary>Change the picture for the next tetro</summary>
	void ChangeNextTetro(int a_TypeID);

private:
	MessageBox* m_ScoreBox = nullptr;
	MessageBox* m_LevelBox = nullptr;
	MessageBox* m_TimerBox = nullptr;
	MessageBox* m_LineCleared = nullptr;

	MessageBox* m_InputsAction = nullptr;
	MessageBox* m_InputsMove = nullptr;
	
	MessageBox* m_PauseBox = nullptr;
	MessageBox* m_GameOver = nullptr;

	UIBox* m_NextTetro = nullptr;
};

