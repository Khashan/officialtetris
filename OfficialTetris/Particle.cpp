#include "Particle.h"
#include "Timer.h"
#include <random>
#include "ParticleManager.h"

Particle::Particle(std::string a_SpritePath, EngineSpriteStruct& a_SpriteStruct, int a_X, int a_Y)
	: m_SpriteStruct(a_SpriteStruct), m_X(a_X), m_Y(a_Y)
{

	//Load Particle File
	LoadFromFile(a_SpritePath);

	InitParticuleSprite();

	m_AnimationTimer = new Timer();
	m_AnimationTimer->Start();

	if (a_SpriteStruct.m_AnimationLifeTime != -1)
	{
		m_SecondTimer = new Timer();
		m_SecondTimer->Start();
	}


	m_Frame = 0;
}

Particle::~Particle()
{
	m_SpriteStruct.m_SpriteClips.clear();
	m_SpriteStruct.m_SpriteSettings = {};
	m_SpriteStruct= {};

	if (m_AnimationTimer != nullptr)
	{
		delete m_AnimationTimer;
		m_AnimationTimer = nullptr;
	}

	if (m_SecondTimer != nullptr)
	{
		delete m_SecondTimer;
		m_SecondTimer = nullptr;
	}
}

void Particle::InitParticuleSprite()
{
	int rectW = GetWidth() / m_SpriteStruct.m_TotalXSprites;
	int rectH = GetHeight() / m_SpriteStruct.m_TotalYSprites;

	m_SpriteStruct.m_SpriteSettings = { 0,0, rectW, rectH };

	SDL_Rect& defaultSprite = m_SpriteStruct.m_SpriteSettings;

	vector<SDL_Rect> sprites;

	for (int j = 1; j <= m_SpriteStruct.m_TotalYSprites; j++)
	{
		for (int i = 1; i <= m_SpriteStruct.m_TotalXSprites; i++)
		{
			SDL_Rect sprite = m_SpriteStruct.m_SpriteSettings;
			sprite.x += i * defaultSprite.w;
			sprite.y += j * defaultSprite.h;

			sprites.push_back(sprite);
		}
	}

	m_SpriteStruct.m_SpriteClips = sprites;
	m_SpriteStruct.m_TotalSprites = sprites.size();
	m_SpriteStruct.m_CurrentClip = sprites[0];
}

void Particle::Draw(SDL_Renderer* a_Renderer)
{
	if (m_IsFinished)
	{
		ParticleManager::GetInstance()->RemoveParticle(this);
		return;
	}

	if (m_SecondTimer != nullptr && m_SecondTimer->GetTicks() > 1000)
	{
		m_SecondTimer->Start();
		m_LifeTime++;

		if (m_SpriteStruct.m_AnimationLifeTime != -1 && m_LifeTime >= m_SpriteStruct.m_AnimationLifeTime)
		{
			m_SecondTimer->Stop();
			m_AnimationTimer->Stop();
			m_IsFinished = true;
		}
	}

	if (!m_IsFinished && m_AnimationTimer != nullptr &&  m_AnimationTimer->GetTicks() > 1000 * m_SpriteStruct.m_AnimationDelay)
	{
		m_AnimationTimer->Start();
		m_Frame++;

		if (m_Frame > m_SpriteStruct.m_TotalSprites - 1)
		{
			if (m_SpriteStruct.m_HasLoop)
			{
				m_Frame = 0;
			}
			else
			{
				m_AnimationTimer->Stop();
				m_IsFinished = true;
				return;
			}
		}
	
		m_SpriteStruct.m_CurrentClip = m_SpriteStruct.m_SpriteClips[m_Frame];
	}

	DrawTexture(m_X, m_Y, &m_SpriteStruct.m_CurrentClip);
}

bool Particle::IsDead()
{
	return m_IsFinished;
}