#pragma once
#include <SDL.h>
#include <string>
#include <vector>
#include "EngineTexture.h"

using std::vector;
using std::string;

class Timer;


class Particle : public EngineTexture
{
public:
	Particle(string a_SpritePath, EngineSpriteStruct& m_SpriteStruct, int a_X, int a_Y);
	~Particle();

	///<summary>Initialize particule</summary>
	void InitParticuleSprite();

	///<summary>Shows the particule</summary>
	void Draw(SDL_Renderer* a_Renderer) override;

	///<summary>Is particle dead</summary>
	bool IsDead();

private:
	int m_X, m_Y;
	int m_Frame;
	bool m_IsFinished = false;
	int m_LifeTime = 0;

	EngineSpriteStruct m_SpriteStruct{};
	Timer* m_AnimationTimer = nullptr;
	Timer* m_SecondTimer = nullptr;
};

