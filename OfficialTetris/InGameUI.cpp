#include "InGameUI.h"
#include "MessageBox.h"
#include "Engine.h"
#include "TetrominoManager.h"
#include <sstream>

#define IMG_I "images/I.png"
#define IMG_J "images/J.png"
#define IMG_L "images/L.png"
#define IMG_O "images/O.png"
#define IMG_S "images/S.png"
#define IMG_Z "images/Z.png"
#define IMG_T "images/T.png"


InGameUI::InGameUI()
{
	Init();
}

InGameUI::~InGameUI()
{
	if (m_ScoreBox != nullptr)
	{
		delete m_ScoreBox;
		m_ScoreBox = nullptr;
	}

	if (m_LevelBox != nullptr)
	{
		delete m_LevelBox;
		m_LevelBox = nullptr;
	}

	if (m_InputsAction != nullptr)
	{
		delete m_InputsAction;
		m_InputsAction = nullptr;
	}

	if (m_InputsMove != nullptr)
	{
		delete m_InputsMove;
		m_InputsMove = nullptr;
	}

	if (m_TimerBox != nullptr)
	{
		delete m_TimerBox;
		m_TimerBox = nullptr;
	}

	if (m_NextTetro != nullptr)
	{
		delete m_NextTetro;
		m_NextTetro = nullptr;
	}

	if (m_LineCleared != nullptr)
	{
		delete m_LineCleared;
		m_LineCleared = nullptr;
	}
}

void InGameUI::Init()
{
	UIBox::Box scoreSettings{ (char*)"Scores", 22, (char*)"fonts/FiraCode-Bold.ttf", SDL_Color{73,167,105}, SDL_Rect{50,Engine::GetInstance()->GetScreenHeight() - 120, 130 , 45}, SDL_Color{106,6,6,255}, SDL_Color{181,188,192,255}, UIBox::BoxBackground::NO_BACKGROUND, UIBox::BoxStyle::BOX, UIBox::BoxContenuPos::TOP_LEFT };
	m_ScoreBox = new MessageBox(scoreSettings, "0", 20, SDL_Color{ 164,201,180 });

	UIBox::Box levelSettings{ (char*)"Level", 22, (char*)"fonts/FiraCode-Bold.ttf", SDL_Color{73,167,105}, SDL_Rect{200,Engine::GetInstance()->GetScreenHeight() - 120, 130 , 45}, SDL_Color{106,6,6,255}, SDL_Color{181,188,192,255 }, UIBox::BoxBackground::NO_BACKGROUND, UIBox::BoxStyle::BOX, UIBox::BoxContenuPos::TOP_LEFT };
	m_LevelBox = new MessageBox(levelSettings, "1", 20, SDL_Color{ 164,201,180 });

	UIBox::Box timerSettings{ (char*)"Time", 22, (char*)"fonts/FiraCode-Bold.ttf", SDL_Color{73,167,105}, SDL_Rect{350,Engine::GetInstance()->GetScreenHeight() - 120, 130 , 45}, SDL_Color{106,6,6,255}, SDL_Color{181,188,192,255 }, UIBox::BoxBackground::NO_BACKGROUND, UIBox::BoxStyle::BOX, UIBox::BoxContenuPos::TOP_LEFT };
	m_TimerBox = new MessageBox(timerSettings, "0", 20, SDL_Color{ 164,201,180 });

	UIBox::Box lineClearnedSetting{ (char*)"Line Cleared", 20, (char*)"fonts/FiraCode-Bold.ttf", SDL_Color{73,167,105}, SDL_Rect{500,Engine::GetInstance()->GetScreenHeight() - 120, 130 , 45}, SDL_Color{106,6,6,255}, SDL_Color{181,188,192,255 }, UIBox::BoxBackground::NO_BACKGROUND, UIBox::BoxStyle::BOX, UIBox::BoxContenuPos::TOP_LEFT };
	m_LineCleared = new MessageBox(lineClearnedSetting, "0", 20, SDL_Color{ 164,201,180 });

	UIBox::Box inputASettings{ nullptr, 0, (char*)"fonts/FiraCode-Light.ttf", SDL_Color{180,177,245}, SDL_Rect{0,Engine::GetInstance()->GetScreenHeight() - 50, Engine::GetInstance()->GetScreenWidth(), 20}, SDL_Color{255,255,255,255}, SDL_Color{255,255,255,255 }, UIBox::BoxBackground::NO_BACKGROUND, UIBox::BoxStyle::NO_STYLE, UIBox::BoxContenuPos::TOP_LEFT };
	m_InputsAction = new MessageBox(inputASettings, "ESC - Pause | SPACE - Start Game | Q - +Rotation | E - -Rotation | P - Cheat (++Multiplier)", 14, SDL_Color{ 182,252,213 });

	UIBox::Box inputMSettings{ nullptr, 0, (char*)"fonts/FiraCode-Light.ttf", SDL_Color{180,177,245}, SDL_Rect{0,Engine::GetInstance()->GetScreenHeight() - 30, Engine::GetInstance()->GetScreenWidth(), 20}, SDL_Color{255,255,255,255}, SDL_Color{255,255,255,255 }, UIBox::BoxBackground::NO_BACKGROUND, UIBox::BoxStyle::NO_STYLE, UIBox::BoxContenuPos::TOP_LEFT };
	m_InputsMove = new MessageBox(inputMSettings, "W/UP - Hard Drop | S/Down - Soft drop | A/Left - Left Move | D/Right - Right Move", 14, SDL_Color{ 182,252,213 });

	UIBox::Box pauseSetting{ nullptr, 0, (char*)"fonts/FiraCode-Light.ttf", SDL_Color{180,177,245}, SDL_Rect{0,0, Engine::GetInstance()->GetScreenWidth(), Engine::GetInstance()->GetScreenHeight()}, SDL_Color{0,0,0,200}, SDL_Color{255,255,255,255 }, UIBox::BoxBackground::COLOR, UIBox::BoxStyle::NO_STYLE, UIBox::BoxContenuPos::TOP_LEFT };
	m_PauseBox = new MessageBox(pauseSetting, "Game Paused (ESC to resume)", 40, SDL_Color{ 182,252,213 });
	m_PauseBox->SetEnable(false);

	UIBox::Box gameOverSetting{ nullptr, 0, (char*)"fonts/FiraCode-Light.ttf", SDL_Color{180,177,245}, SDL_Rect{0,0, Engine::GetInstance()->GetScreenWidth(), Engine::GetInstance()->GetScreenHeight()}, SDL_Color{0,0,0,200}, SDL_Color{255,255,255,255 }, UIBox::BoxBackground::COLOR, UIBox::BoxStyle::NO_STYLE, UIBox::BoxContenuPos::TOP_LEFT };
	m_GameOver = new MessageBox(gameOverSetting, "Game Over...", 25, SDL_Color{ 255,0,0 });
	m_GameOver->SetEnable(false);
}

void InGameUI::ShowUI(SDL_Renderer* a_Renderer)
{
	if (m_ScoreBox != nullptr)
	{
		m_ScoreBox->DrawBox(a_Renderer);
	}

	if (m_LevelBox != nullptr)
	{
		m_LevelBox->DrawBox(a_Renderer);
	}

	if (m_TimerBox != nullptr)
	{
		m_TimerBox->DrawBox(a_Renderer);
	}

	if (m_LineCleared != nullptr)
	{
		m_LineCleared->DrawBox(a_Renderer);
	}

	if (m_InputsMove != nullptr)
	{
		m_InputsMove->DrawBox(a_Renderer);
	}

	if (m_InputsAction != nullptr)
	{
		m_InputsAction->DrawBox(a_Renderer);
	}

	if (m_NextTetro != nullptr)
	{
		m_NextTetro->DrawBox(a_Renderer);
	}

	if (m_PauseBox != nullptr && m_PauseBox->IsEnable())
	{
		m_PauseBox->DrawBox(a_Renderer);
	}

	if (m_GameOver != nullptr && m_GameOver->IsEnable())
	{
		m_GameOver->DrawBox(a_Renderer);
	}

}

void InGameUI::ChangeLevelText(string a_Text)
{
	m_LevelBox->ChangeText(a_Text);
}

void InGameUI::ChangeScoreText(string a_Text)
{
	m_ScoreBox->ChangeText(a_Text);
}

void InGameUI::ChangeTimerText(string a_Text)
{
	m_TimerBox->ChangeText(a_Text);
}

void InGameUI::ChangeLineClearedText(string a_Text)
{
	m_LineCleared->ChangeText(a_Text);
}

void InGameUI::EnablePause(bool a_Enable)
{
	m_PauseBox->SetEnable(a_Enable);
}

void InGameUI::EnableGameOver(bool a_Enable, int& m_Score)
{
	std::ostringstream msg;
	msg << "Game Over! (" << m_Score << ") [Space] to restart.";
	m_GameOver->ChangeText(msg.str());
	m_GameOver->SetEnable(a_Enable);
}

bool InGameUI::IsGameOverActivated()
{
	return m_GameOver->IsEnable();
}

void InGameUI::ChangeNextTetro(int a_TypeID)
{
	TetrominoManager::TetrominoType tType = (TetrominoManager::TetrominoType) a_TypeID;

	UIBox::Box tetroSetting;

	if (m_NextTetro != nullptr)
	{
		tetroSetting = m_NextTetro->GetBox();
	}
	else
	{
		tetroSetting = { (char*)"Next Tetromino", 20, (char*)"fonts/FiraCode-Bold.ttf", SDL_Color{73,167,105}, SDL_Rect{40,Engine::GetInstance()->GetScreenHeight()/2,100,100}, SDL_Color{106,6,6,255}, SDL_Color{181,188,192,255}, UIBox::BoxBackground::IMAGE, UIBox::BoxStyle::BOX, UIBox::BoxContenuPos::TOP_LEFT, IMG_I};
		m_NextTetro = new UIBox(tetroSetting);
	}

	switch (tType)
	{
	case TetrominoManager::I:
		tetroSetting.m_ImgPath = IMG_I;
		break;
	case TetrominoManager::J:
		tetroSetting.m_ImgPath = IMG_J;
		break;
	case TetrominoManager::L:
		tetroSetting.m_ImgPath = IMG_L;
		break;
	case TetrominoManager::O:
		tetroSetting.m_ImgPath = IMG_O;
		break;
	case TetrominoManager::S:
		tetroSetting.m_ImgPath = IMG_S;
		break;
	case TetrominoManager::Z:
		tetroSetting.m_ImgPath = IMG_Z;
		break;
	case TetrominoManager::T:
		tetroSetting.m_ImgPath = IMG_T;
		break;
	default:
		std::cout << "NOT FOUND TYPE!";
		break;
	}

	m_NextTetro->SetBox(tetroSetting);
}