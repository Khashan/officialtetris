#include "UIBox.h"
#include "Engine.h"


UIBox::UIBox(Box a_Box)
{
	SetBox(a_Box);
}

UIBox::~UIBox()
{
	if (m_Box.m_Title != nullptr)
	{
		delete m_Box.m_Title;
	}

	if (m_Box.m_FontPath != nullptr)
	{
		delete m_Box.m_FontPath;
	}

	TTF_CloseFont(m_TitleFont);
	SDL_FreeSurface(m_SurfaceMessage);
	SDL_DestroyTexture(m_MessageSurface);
	SDL_DestroyTexture(m_BackgroundImg);

}

void UIBox::DrawBox(SDL_Renderer* a_Renderer)
{
	DrawBackground(a_Renderer);
	DrawBorder(a_Renderer);
	DrawTitle(a_Renderer);
}

void UIBox::DrawBackground(SDL_Renderer* a_Renderer)
{
	if (m_Box.m_Background == BoxBackground::COLOR)
	{
		SDL_SetRenderDrawColor(a_Renderer, m_Box.m_BackgroundColor.r, m_Box.m_BackgroundColor.g, m_Box.m_BackgroundColor.b, m_Box.m_BackgroundColor.a);
		SDL_RenderFillRect(a_Renderer, &m_Box.m_Rect);
	}
	else
	{
		SDL_RenderCopy(a_Renderer, m_BackgroundImg, nullptr, &m_Box.m_Rect);
	}
}

void UIBox::DrawBorder(SDL_Renderer* a_Renderer)
{
	bool borderDone = false;
	int boxSize = m_Box.m_Rect.w / 2 / 10;
	int stage = 0;
	int boxCount = 0;
	int maxBoxCount = -1;
	int lineSpace = 10;

	SDL_SetRenderDrawColor(a_Renderer, m_Box.m_BorderColor.r, m_Box.m_BorderColor.g, m_Box.m_BorderColor.b, m_Box.m_BorderColor.a);
	switch (m_Box.m_Style)
	{
	case BoxStyle::BOX:
		SDL_Rect border = SDL_Rect{ m_Box.m_Rect.x, m_Box.m_Rect.y, m_Box.m_Rect.w, boxSize }; // TL TR
		SDL_RenderFillRect(a_Renderer, &border);
		border = SDL_Rect{ m_Box.m_Rect.x, m_Box.m_Rect.y, boxSize, m_Box.m_Rect.h }; //TL BL
		SDL_RenderFillRect(a_Renderer, &border);
		border = SDL_Rect{ m_Box.m_Rect.x, m_Box.m_Rect.y + m_Box.m_Rect.h, m_Box.m_Rect.w, boxSize }; //BL BR
		SDL_RenderFillRect(a_Renderer, &border);
		border = SDL_Rect{ m_Box.m_Rect.x + m_Box.m_Rect.w, m_Box.m_Rect.y, boxSize, m_Box.m_Rect.h + boxSize }; // TR BR
		SDL_RenderFillRect(a_Renderer, &border);
		break;

	case BoxStyle::LINE:
		SDL_RenderDrawLine(a_Renderer, m_Box.m_Rect.x, m_Box.m_Rect.y, m_Box.m_Rect.x + m_Box.m_Rect.w, m_Box.m_Rect.y); // TL TR
		SDL_RenderDrawLine(a_Renderer, m_Box.m_Rect.x, m_Box.m_Rect.y, m_Box.m_Rect.x, m_Box.m_Rect.y + m_Box.m_Rect.h); // TL BR
		SDL_RenderDrawLine(a_Renderer, m_Box.m_Rect.x, m_Box.m_Rect.y + m_Box.m_Rect.h, m_Box.m_Rect.x + m_Box.m_Rect.w, m_Box.m_Rect.y + m_Box.m_Rect.h); // BL BR
		SDL_RenderDrawLine(a_Renderer, m_Box.m_Rect.x + m_Box.m_Rect.w, m_Box.m_Rect.y + m_Box.m_Rect.h, m_Box.m_Rect.x + m_Box.m_Rect.w, m_Box.m_Rect.y); // BL TR
		break;

	case BoxStyle::DOUBLE_LINE:
		SDL_RenderDrawLine(a_Renderer, m_Box.m_Rect.x, m_Box.m_Rect.y, m_Box.m_Rect.x + m_Box.m_Rect.w, m_Box.m_Rect.y); // TL TR
		SDL_RenderDrawLine(a_Renderer, m_Box.m_Rect.x, m_Box.m_Rect.y + lineSpace, m_Box.m_Rect.x + m_Box.m_Rect.w, m_Box.m_Rect.y + lineSpace); // TL TR

		SDL_RenderDrawLine(a_Renderer, m_Box.m_Rect.x, m_Box.m_Rect.y, m_Box.m_Rect.x, m_Box.m_Rect.y + m_Box.m_Rect.h); // TL BR
		SDL_RenderDrawLine(a_Renderer, m_Box.m_Rect.x, m_Box.m_Rect.y + lineSpace, m_Box.m_Rect.x, m_Box.m_Rect.y + m_Box.m_Rect.h + lineSpace); // TL BR

		SDL_RenderDrawLine(a_Renderer, m_Box.m_Rect.x, m_Box.m_Rect.y + m_Box.m_Rect.h, m_Box.m_Rect.x + m_Box.m_Rect.w, m_Box.m_Rect.y + m_Box.m_Rect.h); // BL BR
		SDL_RenderDrawLine(a_Renderer, m_Box.m_Rect.x, m_Box.m_Rect.y + m_Box.m_Rect.h + lineSpace, m_Box.m_Rect.x + m_Box.m_Rect.w, m_Box.m_Rect.y + m_Box.m_Rect.h + lineSpace); // BL BR

		SDL_RenderDrawLine(a_Renderer, m_Box.m_Rect.x + m_Box.m_Rect.w, m_Box.m_Rect.y + m_Box.m_Rect.h, m_Box.m_Rect.x + m_Box.m_Rect.w, m_Box.m_Rect.y); // BL TR
		SDL_RenderDrawLine(a_Renderer, m_Box.m_Rect.x + m_Box.m_Rect.w, m_Box.m_Rect.y + m_Box.m_Rect.h + lineSpace, m_Box.m_Rect.x + m_Box.m_Rect.w, m_Box.m_Rect.y + lineSpace); // BL TR
		break;

	default:
		break;

	}
}

void UIBox::DrawTitle(SDL_Renderer* a_Renderer)
{
	if (m_MessageSurface == nullptr)
	{
		m_MessageSurface = SDL_CreateTextureFromSurface(a_Renderer, m_SurfaceMessage);
	}

	SDL_RenderCopy(a_Renderer, m_MessageSurface, nullptr, &m_Message_rect);
}

void UIBox::SetBox(Box& a_Box)
{
	m_Box = a_Box;
	if (m_Box.m_Background == BoxBackground::IMAGE)
	{
		m_BackgroundImg = Engine::GetInstance()->LoadSurface(m_Box.m_ImgPath);
	}

	m_TitleFont = TTF_OpenFont(m_Box.m_FontPath, m_Box.m_TitleSize);
	m_SurfaceMessage = TTF_RenderText_Solid(m_TitleFont, m_Box.m_Title, m_Box.m_TitleColor);

	//Init rect Title
	int tw, th;
	if (!TTF_SizeText(m_TitleFont, m_Box.m_Title, &tw, &th))
	{
		m_Message_rect = SDL_Rect{ (m_Box.m_Rect.x + m_Box.m_Rect.w / 2) - (tw / 2), m_Box.m_Rect.y - th - 2 ,tw, th };
	}
}