#pragma once
#include <SDL.h>
#include <iostream>
class EngineBehaviour
{
public:
	EngineBehaviour();
	~EngineBehaviour();

	///<summary>Enable/Disable Behaviour -> If the engine ignore it or not</summary>
	inline virtual void SetEnable(bool a_Active) { m_IsEnable = a_Active; }
	///<summary>Get Behaviour State</summary>
	inline bool IsEnable() { return m_IsEnable; }

protected:
	///<summary>Draw the component inside the window
	///If no override, it will unsubrscribe automatically</summary>
	/// <param name="a_Renderer">Main renderer system.</param>
	virtual void Draw(SDL_Renderer* a_Renderer);

	///<summary>Update the component
	///If no override, it will unsubrscribe automatically</summary>
	virtual void Update();

	///<summary>Receive SDL Events.
	///If no override, it will unsubrscribe automatically</summary>
	/// <param name="a_Event">The SDL event.</param>
	virtual void EventsHandler(SDL_Event& a_Event);

protected:
	bool m_IsEnable = true;
};

