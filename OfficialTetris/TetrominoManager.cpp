#include "TetrominoManager.h"
#include "Engine.h"
#include "Tetromino.h"
#include "Game.h"

#include <SDL.h>

#define WHITE SDL_Color{0,0,0,255}
#define AQUA SDL_Color{0,255,255,255}
#define BLUE SDL_Color{0,0,255,255}
#define ORANGE SDL_Color{255,165,0,255}
#define YELLOW SDL_Color{255,255,0,255}
#define GREEN SDL_Color{0,255,0,255}
#define PURPLE SDL_Color{128,255,128,255}
#define RED SDL_Color{255,0,0,255}

int TetrominoManager::m_TetroSize = 0;
TetrominoManager::TetrominoManager(Game* a_Game)
{ 
	TetrominoManager::m_TetroSize = Engine::GetInstance()->GetScreenWidth() / 2 / 11 + 1; //700 -> 32px
	m_Game = a_Game;
	
	//Init matrice database of all tetrominos + their rotation
	CreateDatabase();
}


TetrominoManager::~TetrominoManager()
{
	m_Game = nullptr;
}

Tetromino* TetrominoManager::CreatePiece(TetrominoType a_Type)
{
	SDL_Color color = WHITE;

	switch (a_Type)
	{
	case TetrominoManager::I:
		color = AQUA;
		break;

	case TetrominoManager::O:
		color = YELLOW;
		break;

	case TetrominoManager::T:
		color = PURPLE;
		break;

	case TetrominoManager::S:
		color = GREEN;
		break;

	case TetrominoManager::Z:
		color = RED;
		break;

	case TetrominoManager::J:
		color = BLUE;
		break;

	case TetrominoManager::L:
		color = ORANGE;
		break;

	default:
		break;
	}

	return new Tetromino(a_Type, m_DefaultPosition[a_Type][0], m_DefaultPosition[a_Type][1], m_Pieces[a_Type], color, m_Game->GetLevel());
}

Tetromino* TetrominoManager::CreateRandomPiece()
{
	TetrominoType type = (TetrominoType)(rand() % m_TypesCount);
	return CreatePiece(type);
}

void TetrominoManager::CreateDatabase()
{
	m_Pieces[TetrominoManager::O] = {
	   {
		{0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0},
		{0, 0, 2, 1, 0},
		{0, 0, 1, 1, 0},
		{0, 0, 0, 0, 0}
		},
	   {
		{0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0},
		{0, 0, 2, 1, 0},
		{0, 0, 1, 1, 0},
		{0, 0, 0, 0, 0}
		},
	   {
		{0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0},
		{0, 0, 2, 1, 0},
		{0, 0, 1, 1, 0},
		{0, 0, 0, 0, 0}
		},
	   {
		{0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0},
		{0, 0, 2, 1, 0},
		{0, 0, 1, 1, 0},
		{0, 0, 0, 0, 0}
		}
	};
	m_Pieces[TetrominoManager::I] = {
		   {
			{0, 0, 0, 0, 0},
			{0, 0, 1, 0, 0},
			{0, 0, 2, 0, 0},
			{0, 0, 1, 0, 0},
			{0, 0, 1, 0, 0}
			},
		   {
			{0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0},
			{0, 1, 2, 1, 1},
			{0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0}
			},
		   {
			{0, 0, 0, 0, 0},
			{0, 0, 1, 0, 0},
			{0, 0, 2, 0, 0},
			{0, 0, 1, 0, 0},
			{0, 0, 1, 0, 0}
			},
		   {
			{0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0},
			{0, 1, 2, 1, 1},
			{0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0}
			}
	};
	m_Pieces[TetrominoManager::L] = {
		   {
			{0, 0, 0, 0, 0},
			{0, 0, 1, 0, 0},
			{0, 0, 2, 0, 0},
			{0, 0, 1, 1, 0},
			{0, 0, 0, 0, 0}
			},
		   {
			{0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0},
			{0, 1, 2, 1, 0},
			{0, 1, 0, 0, 0},
			{0, 0, 0, 0, 0}
			},
		   {
			{0, 0, 0, 0, 0},
			{0, 1, 1, 0, 0},
			{0, 0, 2, 0, 0},
			{0, 0, 1, 0, 0},
			{0, 0, 0, 0, 0}
			},
		   {
			{0, 0, 0, 0, 0},
			{0, 0, 0, 1, 0},
			{0, 1, 2, 1, 0},
			{0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0}
			}
	};
	m_Pieces[TetrominoManager::J] = {
		   {
			{0, 0, 0, 0, 0},
			{0, 0, 1, 0, 0},
			{0, 0, 2, 0, 0},
			{0, 1, 1, 0, 0},
			{0, 0, 0, 0, 0}
			},
		   {
			{0, 0, 0, 0, 0},
			{0, 1, 0, 0, 0},
			{0, 1, 2, 1, 0},
			{0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0}
			},
		   {
			{0, 0, 0, 0, 0},
			{0, 0, 1, 1, 0},
			{0, 0, 2, 0, 0},
			{0, 0, 1, 0, 0},
			{0, 0, 0, 0, 0}
			},
		   {
			{0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0},
			{0, 1, 2, 1, 0},
			{0, 0, 0, 1, 0},
			{0, 0, 0, 0, 0}
			}
	};
	m_Pieces[TetrominoManager::S] = {
		   {
			{0, 0, 0, 0, 0},
			{0, 0, 0, 1, 0},
			{0, 0, 2, 1, 0},
			{0, 0, 1, 0, 0},
			{0, 0, 0, 0, 0}
			},
		   {
			{0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0},
			{0, 1, 2, 0, 0},
			{0, 0, 1, 1, 0},
			{0, 0, 0, 0, 0}
			},
		   {
			{0, 0, 0, 0, 0},
			{0, 0, 1, 0, 0},
			{0, 1, 2, 0, 0},
			{0, 1, 0, 0, 0},
			{0, 0, 0, 0, 0}
			},
		   {
			{0, 0, 0, 0, 0},
			{0, 1, 1, 0, 0},
			{0, 0, 2, 1, 0},
			{0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0}
			}
	};
	m_Pieces[TetrominoManager::Z] = {
		   {
			{0, 0, 0, 0, 0},
			{0, 0, 1, 0, 0},
			{0, 0, 2, 1, 0},
			{0, 0, 0, 1, 0},
			{0, 0, 0, 0, 0}
			},
		   {
			{0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0},
			{0, 0, 2, 1, 0},
			{0, 1, 1, 0, 0},
			{0, 0, 0, 0, 0}
			},
		   {
			{0, 0, 0, 0, 0},
			{0, 1, 0, 0, 0},
			{0, 1, 2, 0, 0},
			{0, 0, 1, 0, 0},
			{0, 0, 0, 0, 0}
			},
		   {
			{0, 0, 0, 0, 0},
			{0, 0, 1, 1, 0},
			{0, 1, 2, 0, 0},
			{0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0}
			}
	};
	m_Pieces[TetrominoManager::T] = {
		   {
			{0, 0, 0, 0, 0},
			{0, 0, 1, 0, 0},
			{0, 0, 2, 1, 0},
			{0, 0, 1, 0, 0},
			{0, 0, 0, 0, 0}
			},
		   {
			{0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0},
			{0, 1, 2, 1, 0},
			{0, 0, 1, 0, 0},
			{0, 0, 0, 0, 0}
			},
		   {
			{0, 0, 0, 0, 0},
			{0, 0, 1, 0, 0},
			{0, 1, 2, 0, 0},
			{0, 0, 1, 0, 0},
			{0, 0, 0, 0, 0}
			},
		   {
			{0, 0, 0, 0, 0},
			{0, 0, 1, 0, 0},
			{0, 1, 2, 1, 0},
			{0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0}
			}
	};

	//Their position in the matrice vs the origin of the matrice.
	m_DefaultPosition[TetrominoManager::O] = { 3,-2 };
	m_DefaultPosition[TetrominoManager::I] = { 3,-2 };
	m_DefaultPosition[TetrominoManager::L] = { 3,-2 };
	m_DefaultPosition[TetrominoManager::J] = { 3,-1 };
	m_DefaultPosition[TetrominoManager::S] = { 3,-2 };
	m_DefaultPosition[TetrominoManager::Z] = { 3,-2 };
	m_DefaultPosition[TetrominoManager::T] = { 3,-2 };
}