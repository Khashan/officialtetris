#include "Engine.h"
#include "EngineBehaviour.h"
#include "Game.h"
#include "Timer.h"
#include "ParticleManager.h"

#include <SDL_ttf.h>
#include <new>
#include <math.h> 
#include <functional>

//Singleton
Engine* Engine::s_Instance;

Engine::Engine()
{
	s_Instance = this;

	// Init SDL library.
	m_IsEngineRunning = Init();

	//Load Classes
	m_Game = new Game();
	m_Fps = new Timer();
	m_CapTimer = new Timer();
	m_ParticleManager = new ParticleManager();
}

Engine::~Engine()
{
	//clear delegates
	m_UpdateDelegate.clear();
	m_EventsDelegate.clear();
	m_DrawDelegates.clear();

	//Destroy pointers and SDL
	if (m_Game != nullptr)
	{
		delete m_Game;
		m_Game = nullptr;
	}

	if (m_Fps != nullptr)
	{
		delete m_Fps;
		m_Fps = nullptr;
	}

	if (m_CapTimer != nullptr)
	{
		delete m_CapTimer;
		m_CapTimer = nullptr;
	}

	if (m_ParticleManager != nullptr)
	{
		delete m_ParticleManager;
		m_ParticleManager = nullptr;
	}

	SDL_DestroyRenderer(m_Renderer);
	SDL_DestroyWindow(m_Window);

	m_Renderer = nullptr;
	m_Window = nullptr;
	Engine::s_Instance = nullptr;

	TTF_Quit;
	SDL_Quit;
}

void Engine::Start()
{
	//Starts FPS timer
	m_Fps->Start();

	//Game Loop
	while (m_IsEngineRunning)
	{
		m_CapTimer->Start();

		Draw();
		EventsHandler();
		Update();
	}
}

bool Engine::Init()
{
	bool success = true;

//Init SDL
if (SDL_Init(SDL_INIT_VIDEO) < 0)
{
	printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
	success = false;
}
else
{
	//Create Window
	m_Window = SDL_CreateWindow(SCREEN_NAME, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);

	if (m_Window == nullptr)
	{
		printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
		success = false;
	}
	else
	{
		//Init Rendere for window
		m_Renderer = SDL_CreateRenderer(m_Window, -1, SDL_RENDERER_ACCELERATED);

		if (m_Renderer == nullptr)
		{
			printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
			success = false;
		}
		else
		{
			//Contains transparency mode
			SDL_SetRenderDrawBlendMode(m_Renderer, SDL_BLENDMODE_BLEND);

			//Init Renderer Color
			SDL_SetRenderDrawColor(m_Renderer, 255, 255, 255, 255);

			//Inti PNG loading
			int imgFlags = IMG_INIT_PNG;

			if (!(IMG_Init(imgFlags) & imgFlags))
			{
				printf("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
				success = false;
			}
		}

		TTF_Init();
	}
}

return success;
}

void Engine::Draw()
{
	SDL_RenderClear(m_Renderer);

	//Call all the functions inside the Draw delegate
	for (int i = 0; i < m_DrawDelegates.size(); i++)
	{
		bool canDraw = true;

		CallbackStruct<void(SDL_Renderer*)> func = m_DrawDelegates[i];

		if (func.m_Behaviour != nullptr)
		{
			if (!func.m_Behaviour->IsEnable())
				canDraw = false;
		}

		if(canDraw)
			func.m_CallBack(m_Renderer);
	}

	SDL_RenderPresent(m_Renderer);
}

void Engine::EventsHandler()
{
	SDL_Event event;
	SDL_PollEvent(&event);
	
	//Case the event we wants
	switch (event.type)
	{
	case SDL_QUIT:
		m_IsEngineRunning = false;
		break;

	case SDL_KEYDOWN:
		//Call all functions in Events Delegate
		for (int i = 0; i < m_EventsDelegate.size(); i++)
		{
			bool canEvent = true;
			CallbackStruct<void(SDL_Event&)> func = m_EventsDelegate[i];

			if (func.m_Behaviour != nullptr)
			{
				if (!func.m_Behaviour->IsEnable())
					canEvent = false;
			}

			if (canEvent)
			{
				func.m_CallBack(event);
			}
		}
		break;

	default:
		break;
	}
}

void Engine::Update()
{
	//Get avgFps
	float avgFPS = m_Frame / (m_Fps->GetTicks() / 1000.0f);
	if (avgFPS > 2000000)
	{
		avgFPS = 0;
	}

	++m_Frame;
	
	//Limit FPS to 60
	int frameTicks = m_CapTimer->GetTicks();
	if (frameTicks < SCREEN_TICK_PER_FRAME)
	{
		SDL_Delay(SCREEN_TICK_PER_FRAME - frameTicks);
	}

	//Call all functions in Update Delegate
	for(int i = 0; i < m_UpdateDelegate.size(); i++)
	{
		bool canUpdate = true;
		if (m_UpdateDelegate[i].m_Behaviour != nullptr)
		{
			if (!m_UpdateDelegate[i].m_Behaviour->IsEnable())
				canUpdate = false;

		}

		if(canUpdate)
			m_UpdateDelegate[i].m_CallBack();
	}
}

void Engine::SubscribeCallbacks(CallbackStruct<void()>& a_Update, CallbackStruct<void(SDL_Event&)>& a_EventFunc, CallbackStruct<void(SDL_Renderer*)>& a_DrawFunc)
{
	//Check if does subscribe to one of the delegate
	bool addUpdate = (a_Update.m_CallBack != nullptr);
	bool addEvent = (a_EventFunc.m_CallBack != nullptr);
	bool addDraw = (a_DrawFunc.m_CallBack != nullptr);

	#pragma region Verification of duplica
	//Like the region says: Verificaiton of duplica
	if (addUpdate)
	{
		for (CallbackStruct<void()>& cp : m_UpdateDelegate)
		{
			if (cp.m_ID == a_Update.m_ID)
			{
				addUpdate = false;
				break;
			}
		}
	}

	if (addEvent)
	{
		for (CallbackStruct<void(SDL_Event&)>& func : m_EventsDelegate)
		{
			if (&func == &a_EventFunc)
			{
				addEvent = false;
				break;
			}
		}
	}

	if (addDraw)
	{
		for (CallbackStruct<void(SDL_Renderer*)>& func : m_DrawDelegates)
		{
			if (func.m_ID == a_DrawFunc.m_ID)
			{
				addDraw = false;
				break;
			}
		}
	}
#pragma endregion

	//Add them if possible into the delegate's vector
	if (addUpdate)
	{
		m_UpdateDelegate.push_back(a_Update);
	}

	if (addEvent)
	{
		m_EventsDelegate.push_back(a_EventFunc);
	}

	if (addDraw)
	{
		m_DrawDelegates.push_back(a_DrawFunc);
	}

}

void Engine::UnsubscribeCallbacks(void* a_Class)
{
	//Remove the class from the callback
	UnsubscribeUpdateCallbacks(a_Class);
	UnsubscribeEventHandlerCallbacks(a_Class);
	UnsubscribeDrawCallbacks(a_Class);
}

void Engine::UnsubscribeUpdateCallbacks(void* a_ClassPtr)
{
	for (int i = 0; i < m_UpdateDelegate.size(); i++)
	{
		if (m_UpdateDelegate[i].m_ID == a_ClassPtr)
		{
			m_UpdateDelegate.erase(m_UpdateDelegate.begin() + i);
		}
	}
}

void Engine::UnsubscribeEventHandlerCallbacks(void* a_ClassPtr)
{
	for (int i = 0; i < m_EventsDelegate.size(); i++)
	{
		if (m_EventsDelegate[i].m_ID == a_ClassPtr)
		{
			m_EventsDelegate.erase(m_EventsDelegate.begin() + i);
		}
	}
}

void Engine::UnsubscribeDrawCallbacks(void* a_ClassPtr)
{
	for (int i = 0; i < m_DrawDelegates.size(); i++)
	{
		if (m_DrawDelegates[i].m_ID == a_ClassPtr)
		{
			m_DrawDelegates.erase(m_DrawDelegates.begin() + i);
		}
	}
}

SDL_Texture* Engine::LoadSurface(std::string path)
{
	SDL_Texture* img = IMG_LoadTexture(m_Renderer, path.c_str());
	if (img == nullptr)
	{
		printf("Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError());
	}
	else
	{
		return img;
	}

	return nullptr;
}
